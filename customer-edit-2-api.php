<?php
require __DIR__. '/config/init.php';

//echo json_encode($_POST);

$output = [
  'success' => false,
  'code' => 0,
  'error' => '',
];

if(empty($_POST['Name'])){
    $output['code'] = 400;
    $output['error'] = '沒有姓名欄位資料';
    echo json_encode($output); exit;
}


$c_sql = "UPDATE `customers` SET `Name`=?, `Remark`=?, `email`=?, `Introducer`=? WHERE `CustomerID`=?";
$c_stmt = $pdo->prepare($c_sql);
$c_stmt->execute([
    $_POST['Name'],
    $_POST['Remark'],
    $_POST['email'],
    $_POST['Introducer'],
    $_POST['CustomerID'],
]);


if(empty($_POST['PhoneType'])){
    $output['code'] = 430;
    $output['error'] = '沒有電話資料';
    echo json_encode($output); exit;
}

$pd_sql = "DELETE FROM `phonebook` WHERE `CustomerID`=". intval($_POST['CustomerID']);
$pdo->query($pd_sql);  // 移除電話資料

$p_sql = "INSERT INTO `phonebook`(`CustomerID`, `Type`, `Phone`, `Ext`, `Remark`) VALUES (?, ?, ?, ?, ?)";
$p_stmt = $pdo->prepare($p_sql);

foreach($_POST['PhoneType'] as $k=>$v){
    $p_stmt->execute([
        $_POST['CustomerID'],
        $v,
        $_POST['Phone'][$k],
        $_POST['Ext'][$k],
        $_POST['PhoneRemark'][$k],
    ]);
}

// - 地址
$ad_sql = "DELETE FROM `addrbook` WHERE `CustomerID`=". intval($_POST['CustomerID']);
$pdo->query($ad_sql);  // 移除地址資料

$a_sql = "INSERT INTO `addrbook`(
    `CustomerID`, `City`, `Addr`, `Type`, 
    `Floor`, `Elevator`, `ElevatorSize`, `Wooden`, 
    `Parking`, `Remark`
    ) VALUES (
    ?, ?, ?, ?,
    ?, ?, ?, ?,
    ?, ?
    )";
$a_stmt = $pdo->prepare($a_sql);

if(! empty($_POST['City'])){
    foreach($_POST['City'] as $k=>$v){
        $a_stmt->execute([
            $_POST['CustomerID'],
            $v,
            $_POST['Addr'][$k],
            $_POST['HouseType'][$k],

            $_POST['Floor'][$k],
            $_POST['Elevator'][$k],
            $_POST['ElevatorSize'][$k],
            $_POST['Wooden'][$k],

            $_POST['Parking'][$k],
            $_POST['AddressRemark'][$k],
        ]);
    }
}


$output['code'] = 200;
$output['success'] = true;
$output['error'] = '';
echo json_encode($output);




