<style>
    .navbar-nav .nav-item.active {
        background-color: lightskyblue;
        border-radius: 10px;

    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="<?= WEB_ROOT ?>">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= WEB_ROOT ?>">Home</a>
            </li>
            <li class="nav-item <?= $pageName == 'customer-list' ? 'active' : '' ?>">
                <a class="nav-link" href="<?= WEB_ROOT ?>customer-list-2.php">客戶列表</a>
            </li>
            <li class="nav-item <?= $pageName == 'customer-insert' ? 'active' : '' ?>">
                <a class="nav-link" href="<?= WEB_ROOT ?>customer-insert-2.php">新增客戶資料</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="<?= WEB_ROOT ?>tag-list.php">標籤列表</a>
            </li>
            <li class="nav-item <?= $pageName == 'product-list' ? 'active' : '' ?>">
                <a class="nav-link" href="<?= WEB_ROOT ?>product-list.php">產品列表</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="<?= WEB_ROOT ?>product-insert.php">新增產品</a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <?php if(isset($_SESSION['admin'])){ ?>
                <li class="nav-item">
                    <a class="nav-link" ><?= $_SESSION['admin']['nickname'] ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= WEB_ROOT ?>logout-admin.php">登出</a>
                </li>
            <?php }else{ ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= WEB_ROOT ?>login-admin.php">登入</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">會員註冊</a>
                </li>
            <?php } ?>

        </ul>
    </div>
</nav>