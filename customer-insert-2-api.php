<?php
/*
 *
ALTER TABLE `addrbook` CHANGE `Elevator` `Elevator` INT NULL DEFAULT NULL;
ALTER TABLE `addrbook` CHANGE `Wooden` `Wooden` INT NULL DEFAULT NULL;
ALTER TABLE `addrbook` CHANGE `Parking` `Parking` INT NULL DEFAULT NULL;
 */
require __DIR__. '/config/init.php';

//echo json_encode($_POST);

$output = [
  'success' => false,
  'code' => 0,
  'error' => '',
];

if(empty($_POST['Name'])){
    $output['code'] = 400;
    $output['error'] = '沒有姓名欄位資料';
    echo json_encode($output); exit;
}

if(empty($_POST['PhoneType'])){
    $output['code'] = 430;
    $output['error'] = '沒有電話資料';
    echo json_encode($output); exit;
}


$c_sql = "INSERT INTO `customers`(`Name`, `Remark`, `email`) VALUES (?, ?, ?)";
$c_stmt = $pdo->prepare($c_sql);
$c_stmt->execute([
    $_POST['Name'],
    $_POST['Remark'],
    $_POST['email'],
]);

if(! $c_stmt->rowCount()){
    $output['code'] = 410;
    $output['error'] = '客戶資料新增失敗';
    echo json_encode($output); exit;
}

$customer_id = $pdo->lastInsertId();


$p_sql = "INSERT INTO `phonebook`(`CustomerID`, `Type`, `Phone`, `Ext`, `Remark`) VALUES (?, ?, ?, ?, ?)";
$p_stmt = $pdo->prepare($p_sql);

foreach($_POST['PhoneType'] as $k=>$v){
    $p_stmt->execute([
        $customer_id,
        $v,
        $_POST['Phone'][$k],
        $_POST['Ext'][$k],
        $_POST['PhoneRemark'][$k],
    ]);
}

$a_sql = "INSERT INTO `addrbook`(
`CustomerID`, `City`, `Addr`, `Type`, `Floor`, 
`Elevator`, `ElevatorSize`, `Wooden`, `Parking`, `Remark`
) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
$a_stmt = $pdo->prepare($a_sql);

if(! empty($_POST['Addr'])){
    foreach($_POST['Addr'] as $k=>$v){
        if(! empty($v)) {
            $a_stmt->execute([
                $customer_id,
                $_POST['City'][$k],
                $v,
                $_POST['HouseType'][$k],
                $_POST['Floor'][$k],

                $_POST['Elevator'][$k],
                $_POST['ElevatorSize'][$k],
                $_POST['Wooden'][$k],
                $_POST['Parking'][$k],
                $_POST['AddressRemark'][$k],
            ]);
        }
    }
}


$output['code'] = 200;
$output['success'] = true;
$output['error'] = '';
echo json_encode($output);




