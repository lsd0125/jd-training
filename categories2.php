<?php
require __DIR__ . '/config/init.php';
require __DIR__ . '/get-tag-data-functions.php';

$sql = "SELECT `name` `text`, `sid`, `parent_sid`, `url` FROM categories ORDER BY `sequence` ";

$rows = $pdo->query($sql)->fetchAll();

$catDict = [];

// sid 對應到項目
foreach ($rows as $k => $v) {
    $catDict[$v['sid']] = &$rows[$k];  // 設定參照
}

$catTree = [];
foreach ($catDict as $sid => $item) {
    if ($item['parent_sid'] != 0) {
        $catDict[$item['parent_sid']]['nodes'][] = &$catDict[$sid];
    } else {
        $catTree[] = &$catDict[$sid];
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-treeview/bootstrap-treeview.css">
    <link rel="stylesheet" href="fontawesome/css/all.css">
</head>
<body>
<div id="tree"></div>

<script src="js/jquery-3.5.1.js"></script>
<script src="bootstrap-treeview/bootstrap-treeview.js"></script>
<script>
    const data = <?= json_encode($catTree, JSON_UNESCAPED_UNICODE); ?>;

    $('#tree').treeview({data: data});


</script>
</body>
</html>