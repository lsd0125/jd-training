<?php

class Person {
    protected $name = '';
    function __construct($name='nobody')
    {
        $this->name = $name;
    }
    // setter
    function setName($name){
        $this->name = $name;
    }
    // getter
    function getName(){
        return $this->name;
    }
}

class Salesman extends Person {
    protected $id = '';

    function __construct($name = 'nobody', $id='')
    {
        parent::__construct($name);
        $this->id = $id;
    }

    function getInfo(){
        return "{$this->name}: {$this->id} ";
    }

    function getName(){
        return strtoupper($this->name);
    }
}

$p1 = new Person;  // instance 實體，實例
// echo $p1->name. "\n";
$p1->setName('Aaron');
echo $p1->getName(). "\n";

$p2 = new Salesman('David', 'A023');
echo $p2->getInfo(). "\n";
echo $p2->getName(). "\n";