<?php
require __DIR__. '/config/init.php';

$sql = "INSERT INTO `tags`(`name`, `parent_sid`, `sequence`, `visible`) VALUES (?, ?, 1000, 1)";
$stmt = $pdo->prepare($sql);
try {
    $stmt->execute([
        $_POST['name'],
        $_POST['parent_sid'],
    ]);
} catch (\PDOException $ex){
    echo json_encode([
        'success' => false,
        'info' => 'duplicated tag name',
    ]); exit;
}
if($stmt->rowCount()==1){
    echo json_encode([
        'success' => true,
        'info' => 'tag added',
    ]);
} else {
    echo json_encode([
        'success' => false,
        'info' => 'something wrong',
    ]);
}