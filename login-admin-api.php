<?php
require __DIR__. '/__connect_db.php';

$output = [
    'success' => false,
    'postData' => $_POST,
    'errorCode' => 0,
    'error' => ''
];

if(empty($_POST['account']) or empty($_POST['password'])){
    $output['error'] = '必要欄位不足';
    echo json_encode($output, JSON_UNESCAPED_UNICODE);
    exit;
}
// header('Content-Type: text/plain'); // 用檔頭告訴用戶端，內容為純文字
// print_r($_POST);

// TODO: 檢查資料的格式

$sql = "SELECT * FROM admins WHERE account=? AND password=SHA1(?)";

$stmt = $pdo->prepare($sql);

$stmt->execute([
    $_POST['account'],
    $_POST['password'],
]);

if($stmt->rowCount()==1){
    $_SESSION['admin'] = $stmt->fetch();
    $output['success'] = true;
} else {
    $output['error'] = '帳號或密碼錯誤';
}

header('Content-Type: application/json'); // 用檔頭告訴用戶端，內容為JSON
echo json_encode($output, JSON_UNESCAPED_UNICODE);




