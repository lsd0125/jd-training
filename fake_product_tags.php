<?php

require __DIR__. '/config/init.php';

$pdo->beginTransaction();

// 取得 tags 第二層的 sid
$sql = "SELECT `sid` FROM `tags` WHERE parent_sid != 0 ";
$tags2nd = $pdo->query($sql)->fetchAll(PDO::FETCH_NUM);

$tags2nd = array_merge(...$tags2nd);

// 取得所有產品的 PrdID
$product_sids = $pdo->query("SELECT PrdID FROM products")->fetchAll(PDO::FETCH_NUM);
$product_sids = array_merge(...$product_sids);

foreach($product_sids as $PrdID){
    shuffle($tags2nd);

    for($i=0; $i<4; $i++){
        $pdo->query("INSERT INTO `product_tags`(`product_sid`, `tag_sid`)
            VALUES ($PrdID, {$tags2nd[$i]})");
    }
}

$pdo->commit();

echo 'ok';
