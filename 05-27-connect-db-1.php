<?php
require __DIR__. '/__connect_db.php';

$keyword = '信';

$stmt = $pdo->query("SELECT * FROM `customers` LIMIT 20, 10");

$rows = $stmt->fetchAll();


?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>

<div class="container">
    <pre>
        <?php //print_r($rows) ?>
    </pre>
<table class="table">
    <thead>
        <tr>
            <td width="10%">客戶編號</td>
            <td width="10%">姓名</td>
            <td width="10%">電話</td>
            <td width="10%">手機</td>
            <td width="10%">傳真</td>
            <td width="10%">電郵</td>
            <td>地址</td>
        </tr>
    </thead>
  <tbody>
      <?php foreach($rows as $r): ?>
        <tr>
            <td><?= $r['客戶編號'] ?></td>
            <td><?= $r['姓名'] ?></td>
            <td><?= $r['電話'] ?></td>
            <td><?= $r['手機'] ?></td>
            <td><?= $r['傳真'] ?></td>
            <td><?= $r['電郵'] ?></td>
            <td><?= $r['地址'] ?></td>
        </tr>
        <?php endforeach ?>
  </tbody>
</table>

</div>

<?php include __DIR__. '/__scripts.php'  ?>
<?php include __DIR__. '/__html_foot.php'  ?>