<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>
<?php
$sales = [
    [
        'name' => 'bill',
        'age' => 25,
        'gender' => 'male',
    ],
    [
        'name' => 'david',
        'age' => 27,
        'gender' => 'male',
    ],
    [
        'name' => 'flora',
        'age' => 23,
        'gender' => 'female',
    ],
];


?>
<div class="container">
<table class="table">
    <thead>
        <tr>
            <td>姓名</td>
            <td>年齡</td>
            <td>性別</td>
        </tr>
    </thead>
  <tbody>
    <?php foreach($sales as $v){  ?>
    <tr>
        <td><?= $v['name'] ?></td>
        <td><?= $v['age'] ?></td>
        <td><?= $v['gender'] ?></td>
    </tr>
    <?php }  ?>
  </tbody>
</table>

</div>

<?php include __DIR__. '/__scripts.php'  ?>
<?php include __DIR__. '/__html_foot.php'  ?>