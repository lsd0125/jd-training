<?php
require __DIR__. '/config/init.php';
$pageName = 'customer-list-2';

$city = $_GET['city'] ?? '';
$type = $_GET['type'] ?? 'name';

$urlParams = [];
$where = ' WHERE 1 ';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; // 關鍵字搜尋
if(! empty($keyword)){
    $urlParams['keyword'] = $keyword;
    $urlParams['type'] = $type;
    $k = $pdo->quote("%${keyword}%");

    if($type==='name'){
        $where .= " AND c.`Name` LIKE $k ";
    } else {
        $where .= " AND p.`Phone` LIKE $k ";
    }
}

if(! empty($city)){
    $urlParams['city'] = $city;
    $c = $pdo->quote($city);
    $where .= " AND a.`City`=$c ";
}

$page = isset($_GET['page']) ? intval($_GET['page']) : 1; // 用戶要看第幾頁
if($page<1){
    header('Location: customer-list-2.php');
    exit;
}

$perPage = 20;

//$t_sql = "SELECT COUNT(1) num FROM `customers` $where";
$t_sql = "SELECT COUNT(1) num FROM (
    SELECT c.CustomerID FROM `customers` c
        JOIN `phonebook` p ON c.CustomerID=p.CustomerID
        JOIN `addrbook` a ON c.CustomerID=a.CustomerID
        $where
        GROUP BY c.CustomerID
) t;";
$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];

$totalPages = ceil($totalRows/$perPage);
if($page > $totalPages){
    header('Location: customer-list.php?page='. $totalPages);
    exit;
}

//$sql = sprintf("SELECT * FROM `customers` %s ORDER BY `CustomerID` DESC LIMIT %s, %s", $where, ($page-1)*$perPage, $perPage);
$sql = sprintf("SELECT c.* FROM `customers` c
    JOIN `phonebook` p ON c.CustomerID=p.CustomerID
    JOIN `addrbook` a ON c.CustomerID=a.CustomerID
    %s
    GROUP BY c.CustomerID
    ORDER BY c.CustomerID DESC
    LIMIT %s, %s", $where, ($page-1)*$perPage, $perPage);
$rows_c = $pdo->query($sql)->fetchAll();

foreach($rows_c as $v){
    $ids[] = $v['CustomerID'];
}

//print_r($ids); exit; // 查看內容

if(! empty($ids)){
    $sql2 = sprintf("SELECT * FROM `phonebook` WHERE CustomerID IN (%s)", implode(',', $ids));
    $rows_p = $pdo->query($sql2)->fetchAll();

    $sql3 = sprintf("SELECT * FROM `addrbook` WHERE CustomerID IN (%s)", implode(',', $ids));
    $rows_a = $pdo->query($sql3)->fetchAll();
}

?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>

<div class="container">

<div class="row">
    <div class="col">
        <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                <a class="page-link" href="?<?php
                $urlParams['page'] = $page-1;
                echo http_build_query($urlParams); ?>">Previous</a>
            </li>

            <?php for($i=$page-5; $i<=$page+5; $i++){ 
                if($i<1) continue;
                if($i>$totalPages) continue;
                $urlParams['page'] = $i;
                ?>
            <li class="page-item <?= $page==$i ? 'active' : '' ?>">
                <a class="page-link" href="?<?= http_build_query($urlParams) ?>"><?= $i ?></a>
            </li>
            <?php } ?>
            <li class="page-item <?= $page==$totalPages ? 'disabled' : '' ?>">
                <a class="page-link" href="?<?php
                $urlParams['page'] = $page+1;
                echo http_build_query($urlParams); ?>">Next</a>
            </li>
        </ul>
        </nav>
    </div>
    <div class="col">
        <form class="form-inline my-2 my-lg-0">
            <select class="form-control mr-sm-2" name="city">
                <option value="">所有縣市</option>
                <?php foreach($cities as $k=>$c): ?>
                    <option value="<?= $k ?>" <?= $city==$k ? 'selected' : '' ?>><?= $c ?></option>
                <?php endforeach; ?>
            </select>

            <select class="form-control mr-sm-2" name="type">
                <option value="name" <?= $type=='name' ? 'selected' : '' ?>>姓名搜尋</option>
                <option value="tel" <?= $type=='tel' ? 'selected' : '' ?>>電話搜尋</option>
            </select>

            <input class="form-control mr-sm-2" type="search"
                   name="keyword" value="<?= htmlentities($keyword) ?>"
                   placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜尋</button>
        </form>
    </div>
</div>
<table class="table">
    <thead>
        <tr>
            <td><i class="fas fa-trash-alt"></i></td>
            <td>客戶編號</td>
            <td>姓名</td>
            <td>電話</td>
            <td>電郵</td>
            <td>地址</td>

            <td><i class="fas fa-edit"></i></td>
        </tr>
    </thead>
  <tbody>
      <?php foreach($rows_c as $r): ?>
        <tr>
            <td><a href="javascript: del_it(<?= $r['CustomerID'] ?>)"><i class="fas fa-trash-alt"></i></a></td>
            <td><?= $r['CustomerID'] ?></td>
            <td><?= $r['Name'] ?></td>
            <td><?php
                foreach($rows_p as $p){
                    if($r['CustomerID']===$p['CustomerID']){
                        echo $phoneTypes[$p['Type']]. ': '. $p['Phone']. '<br>';
                    }
                }
                ?></td>
            <td><?= $r['email'] ?></td>
            <td><?php
                foreach($rows_a as $a){
                    if($r['CustomerID']===$a['CustomerID']){
                        echo $cities[$a['City']]. ': '. $a['Addr']. '<br>';
                    }
                }
                ?></td>
            <td>
                <a href="customer-edit-2.php?CustomerID=<?= $r['CustomerID'] ?>">
                    <i class="fas fa-edit"></i>
                </a>
            </td>
        </tr>
        <?php endforeach ?>
  </tbody>
</table>

</div>

<?php include __DIR__. '/__scripts.php'  ?>
<script>
    function del_it(id) {

        if(confirm('你要刪除編號為 ' + id + ' 的資料嗎？')){
            location.href = 'del-customer.php?id=' + id;
        }

    }

</script>
<?php include __DIR__. '/__html_foot.php'  ?>