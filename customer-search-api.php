<?php
$pageName = 'customer-list';
require __DIR__. '/__connect_db.php';
$urlParams = [];
$where = ' WHERE 1 ';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; // 關鍵字搜尋
if(! empty($keyword)){
    $k = $pdo->quote("%${keyword}%");
    // $where .= " AND `姓名` LIKE $k OR `手機` LIKE $k";
    $where .= " AND `姓名` LIKE $k ";
    $urlParams['keyword'] = $keyword;
}

$maxItems = 10;


$sql = sprintf("SELECT * FROM `customers` %s ORDER BY `客戶編號` DESC LIMIT %s", $where, $maxItems);
$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll();

echo json_encode($rows, JSON_UNESCAPED_UNICODE);