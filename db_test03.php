<?php
echo microtime(). '<br>';
require '__connect_db.php';

$cc = [
    '台北市' => 1,
    '新北市' => 2,
    '宜蘭縣' => 3,
];

$sql ="SELECT `客戶編號`, `地址` FROM `customers`";

$rows = $pdo->query($sql)->fetchAll();

//echo json_encode($rows, JSON_UNESCAPED_UNICODE);

$u_sql = "UPDATE `customers` SET `縣市`=?, `area_id`=? WHERE `客戶編號`=?";
$u_stmt = $pdo->prepare($u_sql);
foreach($cc as $c => $n) {
    foreach ($rows as $r) {
        if (stripos($r['地址'], $c) === 0) {
            $u_stmt->execute([$c, $n, $r['客戶編號']]);
        }
    }
}
echo microtime(). '<br>';