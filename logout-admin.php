<?php
session_start();

unset($_SESSION['admin']);  // 只移除 admin

// session_destroy(); // 全部清除

header('Location: /test_proj/customer-list.php');
