<?php require __DIR__. '/config/init.php'; ?>
<?php
require __DIR__. '/get-tag-data-functions.php';
$p_sid = empty($_GET['p_sid']) ? 0 : intval($_GET['p_sid']);
?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>
    <style>
        form>.form-group>label {
            color: darkolivegreen;
            font-weight: bold;
        }
    </style>
<div class="container">

    <div class="d-flex justify-content-center" style="margin-top: 2rem;">
        <div class="col-lg-12">
            <div class="card" style="">
                <div class="card-body">
                    <h5 class="card-title alert alert-info">
                            Add tag
                    </h5>
                    <form name="form1" method="post" onsubmit="return false;">
                        <div class="form-group">
                            <label for="parent_sid">* belong to the tag category</label>
                            <select type="text" class="form-control" id="parent_sid" name="parent_sid">
                                <option value="0">*** be a tag category ***</option>
                                <?php foreach($tag_cates as $tc): ?>
                                    <option value="<?= $tc['sid'] ?>" <?= ($p_sid==$tc['sid']) ? 'selected' : '' ?>><?= $tc['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">* tag name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="button" class="btn btn-primary" onclick="doPost()" id="add_btn">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
<?php include __DIR__. '/__scripts.php'  ?>
<script>
    var add_btn = $('#add_btn');
    var i, s, el;

    function doPost(){
        add_btn.hide();

        $.post('tag-add-api.php', {
            name: $('#name').val(),
            parent_sid: $('#parent_sid').val()
        }, function(data){
            if(data.success){
                alert(data.info);
                location.href = 'tag-list.php';
                return;
            } else {
                alert('****** ' + data.info + ' ******');
            }
            add_btn.show();
        }, 'json');
    }
</script>
<?php include __DIR__. '/__html_foot.php'  ?>