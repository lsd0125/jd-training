<?php

class Person {
    private $name = '';
    function __construct($name='nobody')
    {
        $this->name = $name;
    }
    // setter
    function setName($name){
        $this->name = $name;
    }
    // getter
    function getName(){
        return $this->name;
    }
}

class Salesman extends Person {
    private $id = '';

    function __construct($name = 'nobody', $id='')
    {
        parent::__construct($name);
        $this->id = $id;
    }

    function getInfo(){
        return $this->getName(). ": {$this->id} ";
    }
}

$p1 = new Person;  // instance 實體，實例
// echo $p1->name. "\n";
$p1->setName('Aaron');
echo $p1->getName(). "\n";

$p2 = new Salesman('Bill', 'A023');
echo $p2->getInfo(). "\n";