<?php
require __DIR__. '/config/init.php';

$output = [
    'success' => false,
    'body' => $_POST,  // debug
    'code' => 0,
    'error' => '',
];
if(empty($_POST['PrdID'])){
    $output['error'] = '沒有產品編號';
    echo json_encode($output, JSON_UNESCAPED_UNICODE); exit;
}
$sql = "SELECT * FROM `products` WHERE `PrdID`=". intval($_POST['PrdID']);
$stmt = $pdo->query($sql);
if($stmt->rowCount() < 1){
    $output['code'] = 400;
    $output['error'] = '產品編號錯誤';
    echo json_encode($output, JSON_UNESCAPED_UNICODE); exit;
}

// 檢查哪些是必填的
// PrdName
if(empty($_POST['PrdName'])){
    $output['code'] = 410;
    $output['error'] = '沒有產品名稱';
    echo json_encode($output, JSON_UNESCAPED_UNICODE); exit;
}
/*
if(empty($_POST['tags'])){
    $output['code'] = 420;
    $output['error'] = '沒有選擇產地';
    echo json_encode($output, JSON_UNESCAPED_UNICODE); exit;
}
// tag 16, 17
if(array_search('16', $_POST['tags'])===false
    and
    array_search('17', $_POST['tags'])===false
){
    $output['code'] = 430;
    $output['error'] = '沒有選擇產地';
    echo json_encode($output, JSON_UNESCAPED_UNICODE); exit;
}
*/
// TODO: transaction

$sql = "UPDATE `products` SET 
`PrdName`=?,`PrdNameInd`=?,`Length`=?,`Width1`=?,
`Width2`=?,`Width3`=?,`Width4`=?,`Height`=?,
`thickness`=?,`LegW`=?,`SeatH`=?,`Weight`=?,
`Cube`=?,`Standard`=?,`MadeIn`=?,`Cost`=?,
`Price`=?,`Pricing`=?,`QtySafe`=?, `images`=?,
`visible`=?
WHERE `PrdID`=?";
$stmt = $pdo->prepare($sql);

$stmt->execute([
    $_POST['PrdName'], $_POST['PrdNameInd'], $_POST['Length'], $_POST['Width1'],
    $_POST['Width2'], $_POST['Width3'], $_POST['Width4'], $_POST['Height'],
    $_POST['thickness'], $_POST['LegW'], $_POST['SeatH'], $_POST['Weight'],
    $_POST['Cube'], $_POST['Standard'], $_POST['MadeIn'], $_POST['Cost'],
    $_POST['Price'], $_POST['Pricing'], $_POST['QtySafe'], $_POST['images'],
    $_POST['visible'],
    $_POST['PrdID']
]);

$t1_sql = "DELETE FROM `product_tags` WHERE `product_sid`=". intval($_POST['PrdID']);
$pdo->query($t1_sql);

$t2_sql = "INSERT INTO `product_tags` (`product_sid`, `tag_sid`) VALUES (?, ?)";
$stmt2 = $pdo->prepare($t2_sql);
if(! empty($_POST['tags'])){
    foreach($_POST['tags'] as $v){
        $stmt2->execute([ $_POST['PrdID'], $v ]);
    }
}

$output['success'] = true;
echo json_encode($output);