<?php
require __DIR__. '/config/init.php';

$sql = "UPDATE `tags` SET `name`=?, `parent_sid`=?, `visible`=? WHERE `sid`=?";
$stmt = $pdo->prepare($sql);
try {
    $stmt->execute([
        $_POST['name'],
        $_POST['parent_sid'],
        $_POST['visible'],
        $_POST['sid'],
    ]);
} catch (\PDOException $ex){
    echo json_encode([
        'success' => false,
        'info' => 'duplicated tag name',
    ]); exit;
}
if($stmt->rowCount()==1){
    echo json_encode([
        'success' => true,
        'info' => 'tag modified',
    ]);
} else {
    echo json_encode([
        'success' => false,
        'info' => 'something wrong',
    ]);
}
