<?php
$pageName = 'customer-search';
?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>
<style>
    .myRed {
        color: #FF0000;
    }
    /* input.form-control {
        border-color: red;
    } */
</style>
<div class="container">
<div class="row">
    <div class="col-md-6">

        <!-- <form onsubmit="return false"> -->
        <form name="form1" method="post" onsubmit="doSearch(); return false;">
            <!-- `客戶編號`, `姓名`, `電話`, `手機`, `傳真`, `電郵`, `地址` -->
            <div class="form-group">
                <label for="姓名"><span class="myRed">*</span> 姓名</label>
                <input type="text" class="form-control" id="姓名" name="姓名"
                       onkeyup="doSearch(event)"
                       required>
            </div>

<!--            <button type="submit" class="btn btn-primary">搜尋</button>-->

            <div class="form-group">
                <select class="form-control" id="selectCustomer">
                </select>
            </div>
        </form>

    </div>
</div>

</div>
<?php include __DIR__. '/__scripts.php'  ?>
<script>
    const infobar = $('#infobar');

    function doSearch(event){
        if(event && event.code){
            console.log(event.code, event.keyCode, event.which);
        }

        const val = document.form1.姓名.value;
        $.get(
            'customer-search-api.php',
            {keyword: val},
            function(data){
                console.log(data);
                let str = '';
                for(let i of data){
                    str += `<option value="${i.客戶編號}">${i.姓名} - ${i.手機 || ''}</option>`;
                }
                console.log(str);
                $('#selectCustomer').html(str);

                // for(let index in data){
                //
                // }
            },
            'json'
        );
        return false;
    }

</script>
<?php include __DIR__. '/__html_foot.php'  ?>