<?php
if(! isset($pdo)){
	require __DIR__. '/config/__connect_db_jd2.php';
}

function getTagTree($pdo) {

    $sql = "SELECT * FROM `tags` ORDER BY `sequence`, `parent_sid`, `sid`";
    $rows = $pdo->query($sql)->fetchAll();

    $dict = [];
    foreach($rows as $k=>$v){
        $dict[$v['sid']] = &$rows[$k];
    }

    $tagTree = [];
    foreach($dict as $sid=>$item){
        if($item['parent_sid']!=0){
            $dict[$item['parent_sid']]['children'][] = &$dict[$sid];
        } else {
            $tagTree[] = &$dict[$sid];
        }
    }
    return $tagTree;
}

$tag_cates = getTagTree($pdo);
