<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>
<style>
    .myRed {
        color: #FF0000;
    }
    input.form-control {
        border-color: red;
    }
    #姓名 {
        border-color: green;
    }
</style>
<div class="container">

<div class="row">
    <div class="col-md-6">
        <!-- <form onsubmit="return false"> -->
        <form method="post" action="06-05-insert-data-api.php">
            <!-- `客戶編號`, `姓名`, `電話`, `手機`, `傳真`, `電郵`, `地址` -->
            <div class="form-group">
                <label for="姓名"><span class="myRed">*</span> 姓名</label>
                <input type="text" class="form-control" id="姓名" name="姓名" required>
            </div>
            <div class="form-group">
                <label for="電話">電話</label>
                <input type="text" class="form-control" id="電話" name="電話" pattern="0[2-8]-?\d{7,8}" placeholder="0X-XXXXXXXX">
            </div>
            <div class="form-group">
                <label for="手機">手機</label>
                <input type="text" class="form-control" id="手機" name="手機" pattern="09\d{2}-?\d{3}-?\d{3}" placeholder="09XX-XXX-XXX">
            </div>
            <div class="form-group">
                <label for="傳真">傳真</label>
                <input type="text" class="form-control" id="傳真" name="傳真" pattern="0[2-8]-?\d{7,8}" placeholder="0X-XXXXXXXX">
            </div>
            <div class="form-group">
                <label for="電郵"><span class="myRed">*</span> 電郵</label>
                <input type="text" class="form-control" id="電郵" name="電郵" required>
            </div>
            <div class="form-group">
                <label for="地址">地址</label>
                <textarea id="地址" name="地址" cols="30" rows="5" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">新增資料</button>
        </form>

    </div>
</div>

</div>
<?php include __DIR__. '/__scripts.php'  ?>
<?php include __DIR__. '/__html_foot.php'  ?>