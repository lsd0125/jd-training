<?php
require __DIR__. '/config/init.php';
require __DIR__. '/get-tag-data-functions.php';
$pageName = 'product-edit';

// `PrdID`, `PrdName`, `PrdNameInd`, `Length`, `Width1`,
// `Width2`, `Width3`, `Width4`, `Height`, `thickness`,
// `LegW`, `SeatH`, `Weight`, `Cube`, `Standard`,
// `MadeIn`, `Cost`, `Price`, `Pricing`, `QtySafe`

if (empty($_GET['PrdID'])) {
    header('Location: ./'); // 到列表
    exit;
}

$PrdID = intval($_GET['PrdID']) ?? 0; // 轉換成整數
$pRow = $pdo->query("SELECT * FROM products WHERE PrdID=$PrdID")->fetch();

if (empty($pRow)) {
    header('Location: ./'); // 到列表
    exit;
}

$tag_sids = $pdo
    ->query("SELECT tag_sid FROM product_tags WHERE product_sid=$PrdID")
    ->fetchAll();

//echo json_encode($tag_sids); exit;
?>
<?php include __DIR__ . '/__html_head.php' ?>
<?php require __DIR__ . '/__navbar.php' ?>
    <style>
        .myRed {
            color: #FF0000;
        }
        .img-unit {
            display: inline-block;
            position: relative;
            width: 200px;
            height: 200px;
            background-color: #ffffff;
            border: 1px solid red;

        }
        .img-unit > img {
            position: absolute;
            max-width: 200px;
            max-height: 200px;
        }
        .img-unit > .icon-del,
        .img-unit > .icon-left,
        .img-unit > .icon-right
        {
            position: absolute;
            background-color: #fff;
            cursor: pointer;
        }
        .img-unit > .icon-del {
            color: red;
            top: 0;
            right: 0;
        }
        .img-unit > .icon-left{
            color: blue;
            left: 0;
            bottom: 0;
        }
        .img-unit > .icon-right{
            color: blue;
            bottom: 0;
            right: 0;
        }
        .img-unit i {
            font-size: 2rem;
        }
    </style>
    <div class="container">

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">編輯商品資料</h5>
                        <form name="form1" onsubmit="formCheck(); return false;">
                            <input type="hidden" name="PrdID" value="<?= $PrdID ?>">
                            <div class="form-group">
                                <label for="產品名"><span class="myRed">*</span> 產品名</label>
                                <input type="text" class="form-control" id="產品名" name="PrdName" required
                                       value="<?= htmlentities($pRow['PrdName']) ?>">
                            </div>

                            <div class="form-group">
                                <label for="產地"><span class="myRed">*</span> 產地</label>
                                <select class="form-control form-control-sm" id="MadeIn" name="MadeIn">
                                    <option value="I">印尼</option>
                                    <option value="T">台灣</option>
                                    <option value="C">中國</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="印尼產品名">印尼產品名</label>
                                <input type="text" class="form-control" id="印尼產品名" name="PrdNameInd"
                                       value="<?= htmlentities($pRow['PrdNameInd']) ?>">
                                <div class="form-group">
                                    <label for="長度">長度</label>
                                    <input type="text" class="form-control" id="長度" name="Length"
                                           value="<?= floatval($pRow['Length']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="寬度1">寬度1</label>
                                    <input type="text" class="form-control" id="寬度1" name="Width1"
                                           value="<?= floatval($pRow['Width1'])  ?>">
                                </div>
                                <div class="form-group">
                                    <label for="寬度2">寬度2</label>
                                    <input type="text" class="form-control" id="寬度2" name="Width2"
                                           value="<?= floatval($pRow['Width2']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="寬度3">寬度3</label>
                                    <input type="text" class="form-control" id="寬度3" name="Width3"
                                           value="<?= floatval($pRow['Width3']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="寬度4">寬度4</label>
                                    <input type="text" class="form-control" id="寬度4" name="Width4"
                                           value="<?= floatval($pRow['Width4']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="高度">高度</label>
                                    <input type="text" class="form-control" id="高度" name="Height"
                                           value="<?= floatval($pRow['Height']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="厚度">厚度</label>
                                    <input type="text" class="form-control" id="厚度" name="thickness"
                                           value="<?= floatval($pRow['thickness']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="腳寬">腳寬</label>
                                    <input type="text" class="form-control" id="腳寬" name="LegW"
                                           value="<?= floatval($pRow['LegW']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="座高">座高</label>
                                    <input type="text" class="form-control" id="座高" name="SeatH"
                                           value="<?= floatval($pRow['SeatH']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="重量">重量</label>
                                    <input type="text" class="form-control" id="重量" name="Weight"
                                           value="<?= floatval($pRow['Weight']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="材積數">材積數</label>
                                    <input type="text" class="form-control" id="材積數" name="Cube"
                                           value="<?= floatval($pRow['Cube']) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="標準品">標準品</label>
                                    <select class="form-control form-control-sm" id="Standard" name="Standard">
                                        <option value="1">是</option>
                                        <option value="0">否</option>
                                    </select>
                                </div>
                                <!-- <button class="btn btn-info" type="button" onclick="addStandardItem()"> + 加入地址欄位</button> -->
                            </div>


                    <div class="form-group">
                        <label for="成本">成本</label>
                        <input type="text" class="form-control" id="成本" name="Cost"
                               value="<?= floatval($pRow['Cost']) ?>">
                    </div>
                    <div class="form-group">
                        <label for="價格">價格</label>
                        <input type="text" class="form-control" id="價格" name="Price"
                               value="<?= floatval($pRow['Price']) ?>">
                    </div>
                    <div class="form-group">
                        <label for="定價">定價</label>
                        <input type="text" class="form-control" id="定價" name="Pricing"
                               value="<?= floatval($pRow['Pricing']) ?>">
                    </div>
                    <div class="form-group">
                        <label for="安全庫存量">安全庫存量</label>
                        <input type="text" class="form-control" id="安全庫存量" name="QtySafe"
                               value="<?= floatval($pRow['QtySafe']) ?>">
                    </div>
                    <div class="form-group">
                        <label for="">* tags</label>
                        <table class="table table-bordered table-striped">
                            <tbody>
                            <?php foreach($tag_cates as $tg): ?>
                                <tr>
                                    <td style="color:darkblue"><?= $tg['name'] ?></td>
                                    <td>
                                        <?php if(!empty($tg['children'])) foreach($tg['children'] as $t): ?>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="tag<?= $t['sid'] ?>" name="tags[]" value="<?= $t['sid'] ?>"
                                                    <?php
                                                    foreach($tag_sids as $t_sid){
                                                        if($t['sid']== $t_sid['tag_sid']){
                                                            echo 'checked';
                                                            break;
                                                        }
                                                    }
                                                    ?>>
                                                <label class="form-check-label" for="tag<?= $t['sid'] ?>"><?= $t['name'] ?></label>
                                            </div>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" name="images" value="<?= htmlentities($pRow['images']) ?>">
                    <div class="images-container"></div>
                    <button type="button" class="btn btn-warning"
                            onclick="upload_field.click()">上傳圖檔</button>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="visible">上架</label>
                        <select class="form-control" id="visible" name="visible">
                            <option value="0" <?= $pRow['visible']==0 ? 'selected' : '' ?>>否</option>
                            <option value="1" <?= $pRow['visible']==1 ? 'selected' : '' ?>>是</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">修改資料</button>
                    </form><br>
                    <div id="infobar" class="alert alert-success" role="alert"
                         style="display:none">
                    </div>
                    <form name="upload_form" style="display: none">
                        <input type="text" name="sid" value="<?= $PrdID ?>">
                        <input id="upload_field" type="file" name="my_files[]"
                               multiple accept="image/*">
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php include __DIR__ . '/__scripts.php' ?>
    <script>

        const infobar = $('#infobar');

        function formCheck() {

            $.post(
                'product-edit-api.php',
                $(document.form1).serialize(),
                function (data) {
                    console.log(data);
                    if (data.success) {
                        infobar.removeClass('alert-danger').addClass('alert-success').text('資料修改成功')
                        setTimeout(function () {
                            location.href = 'product-list.php';
                        }, 3000)
                    } else {
                        infobar.removeClass('alert-success')
                        infobar.addClass('alert-danger')
                        infobar.text(data.error || '資料修改失敗')
                    }
                    infobar.slideDown();
                    setTimeout(function () {
                        infobar.slideUp();
                    }, 3000)
                },
                'json'
            );
            return false;
        }
        // images upload

        const upload_field = $('#upload_field');

        upload_field.on('change', function(){
            const fd = new FormData(document.upload_form);

            fetch('upload-images.php', {
                method: 'POST',
                body: fd
            })
            .then(function(response) {
                return response.json()
            })
            .then(obj=>{
                console.log(obj);
                // obj.results
                if(obj.results && obj.results.length){
                    let img_str = '';
                    obj.results.forEach(function(el){
                        img_str += img_unit_tpl(el);
                    });
                    images_container.append(img_str);
                    getImgSequence();
                }
            });
        });


        // images sequence

        const images_container = $('.images-container');

        function img_unit_tpl(f){
            return `
            <div class="img-unit" data-id="${f}">
                <img src="./upload-img/${f}" alt="${f}">
                <div class="icon-del">
                    <i class="fas fa-times-circle" onclick="icon_del_click(event)"></i>
                </div>
                <div class="icon-left">
                    <i class="fas fa-arrow-circle-left" onclick="icon_left_click(event)"></i>
                </div>
                <div class="icon-right">
                    <i class="fas fa-arrow-circle-right" onclick="icon_right_click(event)"></i>
                </div>
            </div>
            `;
        }
        let img_json = document.form1.images.value;
        let img_ar;
        let img_str = '';
        try{
            img_ar = JSON.parse(img_json);
        }catch(ex){
            img_ar = [];
        }
        img_ar.forEach(function(el){
            img_str += img_unit_tpl(el);
        });
        images_container.html(img_str);


        function icon_del_click(event){
            const unit = $(event.target).closest('.img-unit');
            unit.remove();
            getImgSequence();
        }

        function icon_left_click(event){
            const unit = $(event.target).closest('.img-unit');
            const prev = unit.prev();
            console.log(prev);
            if(prev.length){
                unit.after(prev);
                getImgSequence();
            }
        }

        function icon_right_click(event){
            const unit = $(event.target).closest('.img-unit');
            const next = unit.next();
            console.log(next);
            if(next.length){
                unit.before(next);
                getImgSequence();
            }
        }

        function getImgSequence(){
            let ar = [];
            $('.images-container > .img-unit').each(function(){
                ar.push( $(this).attr('data-id') );
            });
            document.form1.images.value = JSON.stringify(ar);
        }
    </script>
<?php include __DIR__ . '/__html_foot.php' ?>