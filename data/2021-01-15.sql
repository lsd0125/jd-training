SELECT CONCAT(`生產單號`, ' ',`商品編號`) FROM `stocks` WHERE 1;

UPDATE `stocks` SET `tmp`=CONCAT(`生產單號`, `商品編號`) WHERE 1;


SELECT
    `sid`, `生產單號`, `商品編號`, `商品備註`, `廠商`, `銷售單號`, `存放點`, `ShelfNo`, `白配`, `狀態`, `PP`, `QC`, `數量`, `OC`, `PriC`
FROM `stocks` GROUP BY `tmp` ORDER BY sid;
SELECT *, COUNT(`sid`) num FROM `stocks` GROUP BY `tmp` ORDER BY num DESC;

SELECT
    `sid`, `生產單號`, `商品編號`, `商品備註`, `廠商`, `銷售單號`, `存放點`, `ShelfNo`, `白配`, `狀態`, `PP`, `QC`, `數量`, `OC`, `PriC`,
    SUM(`數量`) `總計`, COUNT(`sid`) num
FROM `stocks` GROUP BY `tmp` ORDER BY num DESC;