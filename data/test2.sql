



SELECT * FROM `products` JOIN `categories`;

SELECT `products`.*, `categories`.`name` FROM products JOIN `categories` ON `products`.`category_sid`=`categories`.`sid`;

SELECT `products`.*, `categories`.`name` FROM `products` AS p JOIN `categories` AS c ON p.`category_sid`=c.`sid`;

SELECT p.*, c.`name` FROM `products` p JOIN `categories` c ON p.`category_sid`=c.`sid`;

SELECT p.*, c.`name` cate_name FROM `products` p JOIN `categories` c ON p.`category_sid`=c.`sid`;

SELECT p.*, c.`name` cate_name FROM `products` p LEFT JOIN `categories` c ON p.`category_sid`=c.`sid`;

SELECT p.*, c.`name` cate_name
FROM `products` p
LEFT JOIN `categories` c ON p.`category_sid`=c.`sid`
WHERE c.`sid` IS NULL;

SELECT * FROM `orders` o
JOIN `order_details` d ON o.sid=d.order_sid
WHERE o.sid=11;


SELECT * FROM `orders` o
JOIN `order_details` d ON o.sid=d.order_sid
JOIN `members` m ON o.member_sid=m.id
WHERE o.sid=11;

SELECT * FROM `orders` o
JOIN `order_details` d ON o.sid=d.order_sid
JOIN `products` p ON p.sid=d.product_sid
WHERE o.sid=11;
