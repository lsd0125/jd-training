<?php
require __DIR__ . '/config/init.php';
require __DIR__ . '/get-tag-data-functions.php';

?>
<?php include __DIR__ . '/__html_head.php' ?>
    <style>
        /*tr {*/
        /*    background-color: yellow;*/
        /*}*/
    </style>
    <div class="row">
        <div class="col">
<!--            <form class="form-inline my-2 my-lg-0" onsubmit="return false;">-->
                <input class="form-control mr-sm-2"
                       onkeyup="search_keyup()"
                       onclick="search_keyup()"
                       style="width: 50%" type="search" placeholder="篩選標籤" aria-label="Search">
<!--            </form>-->
            <button class="btn btn-outline-success my-2 my-sm-0"
                    type="button" onclick="document.tag_form.submit()">搜尋</button>
        </div>
    </div>
    <div class="form-group">
        <label for="">* tags</label>
        <form name="tag_form" action="product-list.php" target="_parent">
        <table class="table table-bordered table-striped">
            <tbody>
            <?php foreach ($tag_cates as $tg): ?>
                <tr>
                    <td style="color:darkblue"><?= $tg['name'] ?></td>
                    <td>
                        <?php if (!empty($tg['children'])) foreach ($tg['children'] as $t): ?>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="tag<?= $t['sid'] ?>" name="tags[]"
                                       value="<?= $t['sid'] ?>">
                                <label class="form-check-label" for="tag<?= $t['sid'] ?>"><?= $t['name'] ?></label>
                            </div>
                        <?php endforeach; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        </form>
    </div>
    <script src="js/jquery-3.5.1.js"></script>
    <script>
        const tags_cb = document.querySelectorAll('input[name=tags\\[\\]]');
        const search_field = document.querySelector('input[type=search]');
        function search_keyup(){

            setTimeout(function(){
                console.log(search_field.value);

                const s = search_field.value.trim(); // 你要找的內容
                // 不是空字串才往下做
                if(s){
                    tags_cb.forEach(function(el){
                        const txt = el.nextElementSibling.innerHTML;
                        if(txt.indexOf(s) >=0 ){
                            el.parentElement.style.display = 'inline-block';
                        } else {
                            el.parentElement.style.display = 'none';
                        }
                    });
                } else {
                    tags_cb.forEach(function(el){
                        el.parentElement.style.display = 'inline-block';
                    });
                }
            }, 100)
        }
        /*
        function prepareParams(event) {
            const a_tag = event.target;
            console.log($(document.tag_form).serialize());
            const url = 'product-list.php?' + $(document.tag_form).serialize();
            a_tag.setAttribute('href', url);
        }
         */
    </script>
<?php include __DIR__ . '/__html_foot.php' ?>