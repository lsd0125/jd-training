<?php require __DIR__. '/config/init.php'; ?>
<?php require __DIR__. '/get-tag-data-functions.php'; ?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>
    <style>
        form>.form-group>label {
            color: darkolivegreen;
            font-weight: bold;
        }
        a>i.fa-trash-alt {
            color: #95401a;
        }
    </style>
<div class="container">
    <div class="row" style="margin-top: 1rem">
        <div class="col-lg-8">

        </div>
        <div class="col-lg-4">
            <div class="d-flex justify-content-end">
                <a href="tag-add.php" class="btn btn-info">Add tag</a>
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered" style="margin-top: 1rem">
        <thead>
        <tr>
            <th scope="col"><i class="fas fa-trash-alt"></i></th>
            <th scope="col">#</th>
            <th scope="col">tag category name</th>
            <th scope="col">visible</th>
            <th scope="col">sequence</th>
            <th scope="col"><i class="fas fa-edit"></i></th>
            <th scope="col">add tags</th>
            <th scope="col">tags</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($tag_cates as $k=>$row): ?>
        <tr>
            <td><a href="javascript:doRemove(<?= $row['sid'] ?>)"><i class="fas fa-trash-alt"></i></a></td>
            <td><?= $row['sid'] ?></td>
            <td><?= htmlentities($row['name']) ?></td>
            <td><?= $row['visible'] ? '<i class="fas fa-eye"></i>' : '<i class="fas fa-times"></i>' ?></td>

            <td><select class="tags_seq" data-sid="<?= $row['sid'] ?>" data-parent_sid="0">
                    <?php for($i=1; $i<=count($tag_cates); $i++): ?>
                    <option value="<?=$i?>" <?= ($k+1)==$i ? 'selected' : '' ?>><?=$i?></option>
                    <?php endfor; ?>
                </select></td>
            <td><a href="tag-edit.php?sid=<?= $row['sid'] ?>">
                    <i class="fas fa-edit"></i>
                </a></td>
            <td><a class="btn btn-info" href="tag-add.php?p_sid=<?= $row['sid'] ?>">add</a></td>
            <td>
                <?php
                if(! empty($row['children']))
                foreach($row['children'] as $k2=>$tag): ?>
                <p>
                    <a href="javascript:doRemove(<?= $tag['sid'] ?>)"><i class="fas fa-trash-alt"></i></a>
                    <?= $tag['name'] ?>
                    <?= $tag['visible'] ? '<i class="fas fa-eye"></i>' : '<i class="fas fa-times"></i>' ?>
                    <select class="tags_seq" data-sid="<?= $tag['sid'] ?>" data-parent_sid="<?= $tag['parent_sid'] ?>">
                        <?php for($i=1; $i<=count($row['children']); $i++): ?>
                            <option value="<?=$i?>" <?= ($k2+1)==$i ? 'selected' : '' ?>><?=$i?></option>
                        <?php endfor; ?>
                    </select>
                    <a href="tag-edit.php?sid=<?= $tag['sid'] ?>"><i class="fas fa-edit"></i></a>
                </p>
                <?php endforeach; ?>
            </td>

        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php include __DIR__. '/__scripts.php'  ?>
<script>
    $('select.tags_seq').change(function(event){
        var t = $(this);
        var new_seq = t.val(),
            sid = t.attr('data-sid'),
            parent_sid = t.attr('data-parent_sid');

        $.post('tag-sequence-api.php',
            {parent_sid:parent_sid, sid:sid, new_seq:new_seq},
            function(data){
            console.log(data);
            if(data.success){
                location.reload();
            } else {
                alert(data.info);
            }
        }, 'json');
    });

    function doRemove(sid){
        if(! confirm('!!! 確定要移除此項目 ' + sid + ' ?')){
            return;
        }
        $.post('tag-delete-api.php',
			{sid: sid},
			function(data){
            if(data.success){
                location.reload();
            } else {
                alert(data.info);
            }
        }, 'json');
    }
</script>
<?php include __DIR__. '/__html_foot.php'  ?>