<?php
require __DIR__ . '/config/init.php';
require __DIR__ . '/get-tag-data-functions.php';

$sql = "SELECT `name` `text`, `sid`, `parent_sid`, `url` FROM categories ORDER BY `sequence` ";

$rows = $pdo->query($sql)->fetchAll();

$catDict = [];

// sid 對應到項目
foreach ($rows as $k => $v) {
    $catDict[$v['sid']] = &$rows[$k];  // 設定參照
}

$catTree = [];
foreach ($catDict as $sid => $item) {
    if ($item['parent_sid'] != 0) {
        $catDict[$item['parent_sid']]['nodes'][] = &$catDict[$sid];
    } else {
        $catTree[] = &$catDict[$sid];
    }
}


//echo json_encode($rows, JSON_UNESCAPED_UNICODE);
$jsonStr = json_encode($catTree, JSON_UNESCAPED_UNICODE);
echo $jsonStr;

// $f = fopen('categories.json', 'w');
// fwrite($f, $jsonStr);
// fclose($f);


