<?php
require __DIR__. '/__connect_db.php';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1; // 用戶要看第幾頁
$perPage = 20;

$t_sql = "SELECT COUNT(1) num FROM `customers`";
$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];

$totalPages = ceil($totalRows/$perPage);

$sql = sprintf("SELECT * FROM `customers` LIMIT %s, %s", ($page-1)*$perPage, $perPage);
$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll();
/*
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';

if(empty($keyword)){
    $stmt = $pdo->query("SELECT * FROM `customers` LIMIT 0, 20");
} else {
    $k = $pdo->quote("%${keyword}%");
    $sql = "SELECT * FROM `customers` WHERE `姓名` LIKE $k  LIMIT 0, 20";
    $stmt = $pdo->query($sql);
}
$rows = $stmt->fetchAll();

*/
?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>

<div class="container">

<table class="table">
    <thead>
        <tr>
            <td width="10%">刪除</td>
            <td width="10%">客戶編號</td>
            <td width="10%">姓名</td>
            <td width="10%">電話</td>
            <td width="10%">手機</td>
            <td width="10%">傳真</td>
            <td width="10%">電郵</td>
            <td>地址</td>
            <td>修改</td>
        </tr>
    </thead>
  <tbody>
      <?php foreach($rows as $r): ?>
        <tr>
            <td><a href="javascript: del_it(<?= $r['客戶編號'] ?>)">刪除</a></td>
            <td><?= $r['客戶編號'] ?></td>
            <td><?= $r['姓名'] ?></td>
            <td><?= $r['電話'] ?></td>
            <td><?= $r['手機'] ?></td>
            <td><?= $r['傳真'] ?></td>
            <td><?= $r['電郵'] ?></td>
            <td><?= $r['地址'] ?></td>
            <td>修改</td>
        </tr>
        <?php endforeach ?>
  </tbody>
</table>

</div>

<?php include __DIR__. '/__scripts.php'  ?>
<script>
    function del_it(id) {

        if(confirm('你要刪除編號為 ' + id + ' 的資料嗎？')){
            location.href = 'del-customer.php?id=' + id;
        }

    }

</script>
<?php include __DIR__. '/__html_foot.php'  ?>