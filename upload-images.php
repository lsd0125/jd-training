<?php
require __DIR__. '/config/init.php';

$output = [
    'input' => $_FILES,
    'num' => 0,
    'results' => [],
];

$allow_types = [
    'image/jpeg' => '.jpg',
    'image/png' => '.png',
];

if(empty($_POST['sid'])){
    $output['error'] = '沒有產品編號';
    echo json_encode($output);
    exit; // 結束
}

if(! empty($_FILES) and !empty($_FILES['my_files'])){

    foreach($_FILES['my_files']['type'] as $i => $t){
        // 判斷格式是否符合
        if(! empty($allow_types[$t])){
            $uid = uniqid();
            $rnd = rand(100, 999);
            $fname = sprintf("p%'.06d-%s%s%s",
                $_POST['sid'],
                $uid,
                $rnd,
                $allow_types[$t]
            );

            move_uploaded_file(
                $_FILES['my_files']['tmp_name'][$i],
                __DIR__. '/upload-img/'. $fname
            );
            $output['num']++;
            $output['results'][] = $fname;
        }
    }

}
//if($output['num'] > 0){
//    $sql = " UPDATE `products` SET `images`=? WHERE `PrdID`=? ";
//    $stmt = $pdo->prepare($sql);
//    $stmt->execute([
//        json_encode($output['results']),
//        $_POST['sid']
//    ]);
//}

echo json_encode($output);

/*
 *  $_FILES['my_file']['name']   # 原來的檔名
 *  $_FILES['my_file']['type']   # mime-type
 *  $_FILES['my_file']['tmp_name']   # mime-type
 *  $_FILES['my_file']['error']   # 0 表示沒有錯誤
 *  $_FILES['my_file']['size']   # 檔案大小
 */

/*
 *  $_FILES['my_file']['name'][0]   # 原來的檔名
 *  $_FILES['my_file']['type'][0]   # mime-type
 *  $_FILES['my_file']['tmp_name'][0]   # mime-type
 *  $_FILES['my_file']['error'][0]   # 0 表示沒有錯誤
 *  $_FILES['my_file']['size'][0]   # 檔案大小
 *
 *  $_FILES['my_file']['name'][1]   # 原來的檔名
 *  $_FILES['my_file']['type'][1]   # mime-type
 *  $_FILES['my_file']['tmp_name'][1]   # mime-type
 *  $_FILES['my_file']['error'][1]   # 0 表示沒有錯誤
 *  $_FILES['my_file']['size'][1]   # 檔案大小
 */