<?php
require __DIR__ . '/config/init.php';
$pageName = 'customer-edit-2';

if (empty($_GET['CustomerID'])) {
    header('Location: ./'); // 到客戶列表
    exit;
}
$CustomerID = intval($_GET['CustomerID']) ?? 0; // 轉換成整數

$cRow = $pdo->query("SELECT * FROM customers WHERE CustomerID=$CustomerID")->fetch();

if (empty($cRow)) {
    header('Location: ./'); // 到客戶列表
    exit;
}

$phoneRows = $pdo->query("SELECT * FROM `phonebook` WHERE `CustomerID`=$CustomerID")
    ->fetchAll();

$addrRows = $pdo->query("SELECT * FROM `addrbook` WHERE `CustomerID`=$CustomerID")
    ->fetchAll();

?>
<?php include __DIR__ . '/__html_head.php' ?>
<?php require __DIR__ . '/__navbar.php' ?>
<style>
    .myRed {
        color: #FF0000;
    }
    .phone-item .form-control {
        width: auto;
        display: inline-block;
    }
    .address-item {
        border: 1px dotted blue;
        margin-bottom: 5px;
    }
    .address-item .form-control {
        width: auto;
        display: inline-block;
        margin: 2px;
    }
</style>
<div class="container">
    <div id="infobar" class="alert alert-success" role="alert"
         style="display:none">
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">編輯客戶資料</h5>
                    <form name="form1" onsubmit="formCheck(); return false;">
                        <input type="hidden" name="CustomerID" value="<?= $CustomerID ?>">
                        <div class="form-group">
                            <label for="姓名"><span class="myRed">*</span> 姓名</label>
                            <input type="text" class="form-control" id="姓名" name="Name" required
                                   value="<?= htmlentities($cRow['Name']) ?>">
                        </div>
                        <div class="form-group">
                            <label for="介紹人">介紹人</label>
                            <input type="text" class="form-control" id="介紹人" name="Introducer" value="0">
                        </div>
                        <div class="form-group">
                            <label for="聯絡電話">聯絡電話</label>
                            <div id="phones">
                            </div>
                            <button class="btn btn-info" type="button" onclick="addPhoneItem()"> + 加入電話欄位</button>
                        </div>

                        <div class="form-group">
                            <label for="電郵">電郵</label>
                            <input type="text" class="form-control" id="電郵" name="email"
                                   value="<?= htmlentities($cRow['email']) ?>">
                        </div>
                        <div class="form-group">
                            <label for="地址">地址</label>
                            <div id="addresses">
                            </div>
                            <button class="btn btn-info" type="button" onclick="addAddressItem()"> + 加入地址欄位</button>
                        </div>

                        <div class="form-group">
                            <label for="客戶備註">客戶備註</label>
                            <input type="text" class="form-control" id="客戶備註" name="Remark"
                                   value="<?= htmlentities($cRow['Remark']) ?>">
                        </div>

                        <button type="submit" class="btn btn-primary">修改資料</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include __DIR__ . '/__scripts.php' ?>
<script>
    const phones = <?= json_encode($phoneRows) ?>; // 電話資料
    const addrs = <?= json_encode($addrRows) ?>; // 地址資料

    function addPhoneItem(obj = {}) {
        const isMobile = obj.Type === 'M' ? 'selected' : '';
        const isPhone = obj.Type === 'P' ? 'selected' : '';
        const isFax = obj.Type === 'F' ? 'selected' : '';

        $('#phones').append(`
        <div class="phone-item">
            <select class="form-control" name="PhoneType[]">
                <option value="M" ${isMobile}>手機</option>
                <option value="P" ${isPhone}>市話</option>
                <option value="F" ${isFax}>傳真</option>
            </select>
            <input type="text" class="form-control" name="Phone[]" value="${obj.Phone || ''}" pattern="[0-9]{9,}" required>
            <input type="text" class="form-control" name="Ext[]" value="${obj.Ext || ''}" pattern="[0-9]*" placeholder="分機">
            <input type="text" class="form-control" name="PhoneRemark[]" value="${obj.Remark || ''}" placeholder="電話備註">
            <button class="btn btn-warning" type="button" onclick="removePhoneItem(event)"> - </button>
        </div>
        `);
    }
    phones.forEach(function (el) {
        addPhoneItem(el);
    });

    function addAddressItem(obj = {}) {

        $('#addresses').append(`
            <div class="address-item">
                <select class="form-control" name="City[]" data-val="${obj.City}">
                    <?php foreach($cities as $k=>$c): ?>
                    <option value="<?= $k ?>"><?= $c ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="text" class="form-control" name="Addr[]" style="width: 50%"
                       value="${obj.Addr || ''}" placeholder="地址">
                <br>
                <select class="form-control" name="HouseType[]" data-val="${obj.Type}">
                    <?php foreach($houseTypes as $t): ?>
                        <option value="<?= $t ?>"><?= $t ?></option>
                    <?php endforeach; ?>
                </select>

                <input type="number" class="form-control" name="Floor[]"
                    value="${obj.Floor || 1}" placeholder="樓層">
                <br>
                <select class="form-control" name="Elevator[]" data-val="${obj.Elevator}">
                    <option value="1">「有」電梯</option>
                    <option value="0">「無」電梯</option>
                </select>
                <input type="text" class="form-control" name="ElevatorSize[]" style="width: 50%"
                        value="${obj.ElevatorSize || ''}"
                       placeholder="電梯尺寸說明">
                <br>
                <select class="form-control" name="Wooden[]" data-val="${obj.Wooden}">
                    <option value="0">一般地板</option>
                    <option value="1">木質地板</option>
                </select>
                <select class="form-control" name="Parking[]" data-val="${obj.Parking}">
                    <option value="0">不方便停車</option>
                    <option value="1">方便停車</option>
                </select>
                <input type="text" class="form-control" name="AddressRemark[]" style="width: 50%"
                    value="${obj.Remark || ''}" placeholder="地址備註">

                <button class="btn btn-warning" type="button" onclick="removeAddressItem(event)"> - </button>
            </div>
        `);
    }

    addrs.forEach(function (el) {
        addAddressItem(el);
    });

    $('#addresses').find('select').each(function () {
        $(this).val($(this).attr('data-val'));
    });

    const infobar = $('#infobar');

    function formCheck() {
        $.post(
            'customer-edit-2-api.php',
            $(document.form1).serialize(),
            function (data) {
                console.log(data);
                if (data.success) {
                    infobar.removeClass('alert-danger').addClass('alert-success').text('資料修改成功')
                    setTimeout(function () {
                        location.href = 'customer-list-2.php';
                    }, 3000)
                } else {
                    infobar.removeClass('alert-success')
                    infobar.addClass('alert-danger')
                    infobar.text(data.error || '資料修改失敗')
                }
                infobar.slideDown();
                setTimeout(function () {
                    infobar.slideUp();
                }, 3000)
            },
            'json'
        );
        return false;
    }

    function removePhoneItem(event) {
        event.target.closest('.phone-item').remove();
    }

    function removeAddressItem(event) {
        event.target.closest('.address-item').remove();
    }
</script>
<?php include __DIR__ . '/__html_foot.php' ?>