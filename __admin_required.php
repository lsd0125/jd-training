<?php
if(! isset($_SESSION)){
    session_start();
}
if(! isset($_SESSION['admin'])) {
    header('Location: /test_proj/customer-list.php');
    exit;
}

