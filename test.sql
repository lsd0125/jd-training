SELECT * FROM `customers` c
    JOIN `phonebook` p ON c.CustomerID=p.CustomerID
    JOIN `addrbook` a ON c.CustomerID=a.CustomerID;


SELECT * FROM `customers` c
    JOIN `phonebook` p ON c.CustomerID=p.CustomerID
    JOIN `addrbook` a ON c.CustomerID=a.CustomerID
    GROUP BY c.CustomerID;


SELECT COUNT(1) num FROM `customers` c
    JOIN `phonebook` p ON c.CustomerID=p.CustomerID
    JOIN `addrbook` a ON c.CustomerID=a.CustomerID
    GROUP BY c.CustomerID;

SELECT COUNT(1) num FROM (
    SELECT c.CustomerID FROM `customers` c
        JOIN `phonebook` p ON c.CustomerID=p.CustomerID
        JOIN `addrbook` a ON c.CustomerID=a.CustomerID
        GROUP BY c.CustomerID
) t;