<?php
require __DIR__ . '/config/init.php';
$pageName = 'customer-insert-2';
?>
<?php include __DIR__ . '/__html_head.php' ?>
<?php require __DIR__ . '/__navbar.php' ?>
<style>
    .myRed {
        color: #FF0000;
    }
    .phone-item .form-control {
        width: auto;
        display: inline-block;
    }
    .address-item {
        border: 1px dotted blue;
        margin-bottom: 5px;
    }
    .address-item .form-control {
        width: auto;
        display: inline-block;
        margin: 2px;
    }
</style>
<div class="container">
    <div id="infobar" class="alert alert-success" role="alert"
         style="display:none">
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">新增客戶資料</h5>
                    <form name="form1" onsubmit="formCheck(); return false;">
                        <div class="form-group">
                            <label for="姓名"><span class="myRed">*</span> 姓名</label>
                            <input type="text" class="form-control" id="姓名" name="Name" required>
                        </div>
                        <div class="form-group">
                            <label for="介紹人">介紹人</label>
                            <input type="text" class="form-control" id="介紹人" name="Introducer">

                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Launch demo modal
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" style="width: 100%; max-width: 100%; height:100%">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <iframe src="customer-list-2.php" width="100%" height="100%"></iframe>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="form-group">
                            <label for="聯絡電話">聯絡電話</label>
                            <div id="phones">
                            </div>
                            <button class="btn btn-info" type="button" onclick="addPhoneItem()"> + 加入電話欄位</button>
                        </div>

                        <div class="form-group">
                            <label for="電郵">電郵</label>
                            <input type="text" class="form-control" id="電郵" name="email">
                        </div>
                        <div class="form-group">
                            <label for="地址">地址</label>
                            <div id="addresses">
                            </div>
                            <button class="btn btn-info" type="button" onclick="addAddressItem()"> + 加入地址欄位</button>
                        </div>

                        <div class="form-group">
                            <label for="客戶備註">客戶備註</label>
                            <input type="text" class="form-control" id="客戶備註" name="Remark">
                        </div>

                        <button type="submit" class="btn btn-primary">新增資料</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include __DIR__ . '/__scripts.php' ?>
<script>
    function addPhoneItem() {

        $('#phones').append(`
            <div class="phone-item">
                <select class="form-control" name="PhoneType[]">
                    <option value="M">手機</option>
                    <option value="P">市話</option>
                    <option value="F">傳真</option>
                </select>
                <input type="text" class="form-control" name="Phone[]" pattern="[0-9]{9,}" required
                placeholder="電話號碼">
                <input type="text" class="form-control" name="Ext[]" pattern="[0-9]*" placeholder="分機">
                <input type="text" class="form-control" name="PhoneRemark[]" placeholder="電話備註">

                <button class="btn btn-warning" type="button" onclick="removePhoneItem(event)"> - </button>
            </div>
        `);
    }
    addPhoneItem();

    function addAddressItem() {
        $('#addresses').append(`
            <div class="address-item">
                <select class="form-control" name="City[]">
                    <?php foreach($cities as $k=>$c): ?>
                    <option value="<?= $k ?>"><?= $c ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="text" class="form-control" name="Addr[]" style="width: 50%"
                       placeholder="地址">
                <br>
                <select class="form-control" name="HouseType[]">
                    <?php foreach($houseTypes as $t): ?>
                        <option value="<?= $t ?>"><?= $t ?></option>
                    <?php endforeach; ?>
                </select>

                <input type="number" class="form-control" name="Floor[]" placeholder="樓層">
                <br>
                <select class="form-control" name="Elevator[]">
                    <option value="1">「有」電梯</option>
                    <option value="0">「無」電梯</option>
                </select>
                <input type="text" class="form-control" name="ElevatorSize[]" style="width: 50%"
                       placeholder="電梯尺寸說明">
                <br>
                <select class="form-control" name="Wooden[]">
                    <option value="0">一般地板</option>
                    <option value="1">木質地板</option>
                </select>
                <select class="form-control" name="Parking[]">
                    <option value="0">不方便停車</option>
                    <option value="1">方便停車</option>
                </select>
                <input type="text" class="form-control" name="AddressRemark[]" style="width: 50%" placeholder="地址備註">

                <button class="btn btn-warning" type="button" onclick="removeAddressItem(event)"> - </button>
            </div>
        `);
    }
    addAddressItem();

    const infobar = $('#infobar');

    function formCheck() {
        $.post(
            'customer-insert-2-api.php',
            $(document.form1).serialize(),
            function (data) {
                console.log(data);
                if (data.success) {
                    infobar.removeClass('alert-danger').addClass('alert-success').text('資料新增成功')
                    setTimeout(function () {
                        location.href = 'customer-list.php';
                    }, 3000)
                } else {
                    infobar.removeClass('alert-success')
                    infobar.addClass('alert-danger')
                    infobar.text(data.error || '資料新增失敗')
                }
                infobar.slideDown();
                setTimeout(function () {
                    infobar.slideUp();
                }, 3000)
            },
            'json'
        );
        return false;
    }

    function removePhoneItem(event) {
        event.target.closest('.phone-item').remove();
    }

    function removeAddressItem(event) {
        event.target.closest('.address-item').remove();
    }
</script>
<?php include __DIR__ . '/__html_foot.php' ?>