<?php
require __DIR__. '/config/init.php';

$sql = "UPDATE `categories` SET `name`=?, `url`=? WHERE `sid`=?";
$stmt = $pdo->prepare($sql);
try {
    $stmt->execute([
        $_POST['name'],
        $_POST['url'],
        $_POST['sid'],
    ]);
} catch (\PDOException $ex){
    echo json_encode([
        'success' => false,
        'info' => 'error',
    ]); exit;
}
if($stmt->rowCount()==1){
    echo json_encode([
        'success' => true,
        'info' => 'category modified',
    ]);
} else {
    echo json_encode([
        'success' => false,
        'info' => 'something wrong',
    ]);
}
