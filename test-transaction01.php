<?php
require __DIR__. '/config/init.php';

$starttime = microtime(true);

$sql = "INSERT INTO test01 (`name`) VALUES(?)";
$pdo->beginTransaction();
$stmt = $pdo->prepare($sql);
for($i=1; $i<10000; $i++) {
    $rand = sha1(uniqid() . rand());
    $stmt->execute([$rand]);
}
$pdo->commit();
$endtime =  microtime(true);

echo $endtime-$starttime;
