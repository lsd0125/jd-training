<?php
$pageName = 'login-admin';
?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>
<style>
    .myRed {
        color: #FF0000;
    }
    .my-eyes {
        cursor: pointer;
        font-size: 3rem;
    }
    .my-eyes .hidden {
        display: none;
    }
</style>
<div class="container">
    <div id="infobar" class="alert alert-success" role="alert"
         style="display:none">
    </div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">管理者登入</h5>
                <form name="form1" method="post" onsubmit="return formCheck()">
                    <div class="form-group">
                        <label for="account"><span class="myRed">*</span> 帳號</label>
                        <input type="text" class="form-control" id="account" name="account" required>
                    </div>
                    <div class="form-group">
                        <label for="password"><span class="myRed">*</span> 密碼</label>
                        <input type="password" class="form-control" id="password" name="password">
                        <div class="my-eyes">
                            <i class="fas fa-eye"></i>
                            <i class="hidden fas fa-eye-slash"></i>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">登入</button>
                </form>
            </div>
        </div>

    </div>
</div>

</div>
<?php include __DIR__. '/__scripts.php'  ?>
<script>
    const infobar = $('#infobar');
    const myEyes = $('.my-eyes');
    const i2 = $('.my-eyes > *');
    const pw = $('#password');

    myEyes.on('click', function(){
        i2.toggleClass('hidden');
        const h = i2.eq(0).hasClass('hidden');
        console.log( h ); // 第一個是否隱藏
        if(h){
            pw.attr('type', 'text');
        } else {
            pw.attr('type', 'password');
        }
    });


    function formCheck(){
        $.post(
            'login-admin-api.php',
            $(document.form1).serialize(),
            function(data){
                console.log(data);
                if(data.success){
                    infobar.removeClass('alert-danger').addClass('alert-success').text('登入成功')
                    setTimeout(function(){
                        location.href = 'customer-list.php';
                    }, 3000)
                } else {
                    infobar.removeClass('alert-success').addClass('alert-danger');
                    infobar.text(data.error || '登入失敗')
                }
                infobar.slideDown();
                setTimeout(function(){
                    infobar.slideUp();
                }, 3000)
            },
            'json'
        );
        return false;
    }

</script>
<?php include __DIR__. '/__html_foot.php'  ?>