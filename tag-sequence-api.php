<?php
require __DIR__. '/config/init.php';

$result = [
    'success' => false,
    'info' => 'parameters error',
];
if(!isset($_POST['parent_sid']) or empty($_POST['sid']) or empty($_POST['new_seq'])){
    echo json_encode($result); exit;
}

$s_sql = "SELECT * FROM `tags` WHERE `parent_sid`=? AND `sid`<>? ORDER BY `sequence`, `sid`";
$s_stmt = $pdo->prepare($s_sql);
$s_stmt->execute([
    $_POST['parent_sid'],
    $_POST['sid'],
]);
$rows = $s_stmt->fetchAll();
if(empty($rows)){
    $result['info'] = 'no data';
    echo json_encode($result); exit;
}

$pdo->beginTransaction();  // ***
$u_sql = "UPDATE `tags` SET `sequence`=? WHERE `sid`=?";
$u_stmt = $pdo->prepare($u_sql);
foreach ($rows as $k=>$row){
    $u_stmt->execute([
        $k*10 + 10,
        $row['sid'],
    ]);
}
$u_stmt->execute([
    $_POST['new_seq']*10 -5,
    $_POST['sid'],
]);
$pdo->commit();  // ***

echo json_encode([
    'success' => true,
    'info' => 'ok',
]);