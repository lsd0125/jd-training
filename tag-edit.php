<?php require __DIR__. '/config/init.php'; ?>
<?php
require __DIR__. '/get-tag-data-functions.php';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
$i_sql = "SELECT * FROM `tags` WHERE `sid`=$sid";
$row = $pdo->query($i_sql)->fetch();
if(empty($row)){
    header('Location: tag-list.php'); exit;
}
?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>
    <style>
        form>.form-group>label {
            color: darkolivegreen;
            font-weight: bold;
        }
    </style>
<div class="container">

    <div class="d-flex justify-content-center" style="margin-top: 2rem;">
        <div class="col-lg-12">
            <div class="card" style="">
                <div class="card-body">
                    <h5 class="card-title alert alert-danger">
                            Edit tag
                    </h5>
                    <form name="form1" method="post" onsubmit="return false;">
                        <div class="form-group">
                            <label for="parent_sid">* belong to the category</label>
                            <select type="text" class="form-control" id="parent_sid" name="parent_sid">
                                <option value="0">*** a category ***</option>
                                <?php foreach($tag_cates as $tc): ?>
                                    <option value="<?= $tc['sid'] ?>" <?= ($row['parent_sid']==$tc['sid']) ? 'selected' : '' ?>><?= $tc['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">* tag name</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?= htmlentities($row['name']) ?>">
                            <input type="hidden" id="sid" name="sid" value="<?= $row['sid'] ?>">
                        </div>

                        <div class="form-group">
                            <label for="visible">* visible</label>
                            <select class="form-control" id="visible" name="visible">
                                <option value="0" <?= $row['visible']==0 ? 'selected' : '' ?>>no</option>
                                <option value="1" <?= $row['visible']==1 ? 'selected' : '' ?>>yes</option>
                            </select>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="button" class="btn btn-primary" onclick="doPost()" id="edit_btn">Edit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
<?php include __DIR__. '/__scripts.php'  ?>
<script>
    var edit_btn = $('#edit_btn');
    var i, s, el;

    function doPost(){
        edit_btn.hide();

        $.post('tag-edit-api.php', $(document.forms[0]).serialize(), function(data){
            if(data.success){
                alert(data.info);
                location.href = 'tag-list.php';
                return;
            } else {
                alert('****** ' + data.info + ' ******');
            }
            edit_btn.show();
        }, 'json');
    }
</script>
<?php include __DIR__. '/__html_foot.php'  ?>