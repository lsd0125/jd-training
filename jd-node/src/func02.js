
const f1 = function(n){
    return n*n;
};

console.log('---');

const f2 = (n=10, m=10) => m*n;
const f3 = (n) => n*n*n;

module.exports = {f1, f2, f3};
