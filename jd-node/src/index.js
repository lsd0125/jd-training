const express = require('express');

const app = express();

app.set('view engine', 'ejs');

// app.get('/a.html', (req, res)=>{
//     res.send('假的 html');
// });

app.use(express.static('public'));
app.use(express.static(__dirname + '/../../bootstrap'));
app.use(express.static(__dirname + '/../../js'));

app.get('/', (req, res)=>{
    res.render('home', {name: 'shin'});
});

app.get('/abc', (req, res)=>{
    //res.send('abc');
});


app.use((req, res)=>{
    res.send('<h2>404<br>找不到你要的頁面</h2>');
});

app.listen(3000, ()=>{
    console.log('--- server started ---');
})








