class Person {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    
    toJSON(){
        return {
            name: this.name,
            age: this.age,
            test: 123,
        }
    }
    toString(){
        return JSON.stringify(this.toJSON());
    }
    
}
module.exports = Person;

console.log('--- person ---');

