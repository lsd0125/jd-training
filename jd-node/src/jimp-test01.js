const Jimp = require('jimp');


Jimp.read(__dirname + '/../img/dog1.jpg', (error, img)=>{
    console.log(img.bitmap.width, img.bitmap.height);
    img
    .resize(200, Jimp.AUTO)
    .quality(80)
    .greyscale()
    .write(__dirname + '/../img/dog2.jpg');
    
});

