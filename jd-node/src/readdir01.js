const fs = require('fs');
const img_re = /\.(jpg|jpeg|png|gif)$/i;

fs.readdir(__dirname + '/../img', {
    encoding: 'utf8',
    withFileTypes: true,
}, (error, files)=>{
    for(let i of files){
        console.log(i.name, i.isFile() ? 'file' : 'no-file');
        console.log(img_re.test(i.name));
    }
})


