<?php
require __DIR__. '/config/init.php';
require __DIR__ . '/get-tag-data-functions.php';
$pageName = 'product-list';

$urlParams = [];
$type = $_GET['type'] ?? 'PrdName';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; // 關鍵字搜尋
$raw_tags = isset($_GET['tags']) ? $_GET['tags'] : []; // 標籤

// 避免垃圾資料進來
$tags = [];
foreach($raw_tags as $v){
    if(intval($v) > 0){
        $tags[] = intval($v);
    }
}



$where = ' WHERE 1 ';
if(! empty($keyword)){
    $urlParams['keyword'] = $keyword;
    $urlParams['type'] = $type;
    $k = $pdo->quote("%${keyword}%"); // 跳脫

    if($type==='PrdName'){
        $where .= " AND `PrdName` LIKE $k ";
    } else {
        $where .= " AND `PrdNameInd` LIKE $k ";
    }
}


$page = isset($_GET['page']) ? intval($_GET['page']) : 1; // 用戶要看第幾頁
if($page<1){
    header('Location: product-list.php');
    exit;
}
$orderRules = [
    'Price-a' => [
        'rule' => " ORDER BY Price ",
        'name' => '價格：由小到大',
    ],
    'Price-d' => [
        'rule' => " ORDER BY Price DESC ",
        'name' => '價格：由大到小',
    ],
    'Length-a' => [
        'rule' => " ORDER BY Length ",
        'name' => '長度：由小到大',
    ],
    'Length-d' => [
        'rule' => " ORDER BY Length DESC ",
        'name' => '長度：由大到小',
    ],
];

$orderBy = isset($_GET['orderBy']) ? $_GET['orderBy'] : 'Price-a';
$urlParams['orderBy'] = $orderBy;
$orderBySQL = '';

if(! empty($orderRules[$orderBy])){
    $orderBySQL = $orderRules[$orderBy]['rule'];
} else {
    $orderBySQL = $orderRules['Price-a']['rule'];
}

$perPage = 30;

if(count($tags)==0) {
    $t_sql = "SELECT COUNT(1) num FROM `products` $where ";
} elseif(count($tags)==1) {
    $t_sql = sprintf("SELECT COUNT(1) num FROM `products` p 
    JOIN
    (SELECT `product_sid` FROM `product_tags` WHERE tag_sid={$tags[0]}) t
    ON p.PrdID=t.product_sid
    %s ",
        $where);
} else {
    $tags_sql = "SELECT rs1.product_sid FROM (SELECT * FROM `product_tags` WHERE tag_sid={$tags[0]}) rs1 ";

    for($i=0; $i < count($tags)-1; $i++) {
        $tid = $tags[$i + 1];
        $rs = $i + 2;
        $tags_sql .= " JOIN (SELECT * FROM `product_tags` WHERE tag_sid={$tid}) rs{$rs}
            ON rs1.product_sid=rs{$rs}.product_sid ";
    }

    $t_sql = sprintf("SELECT COUNT(1) num FROM `products` p 
    JOIN
    (%s) t
    ON p.PrdID=t.product_sid
    %s ",
        $tags_sql, $where);
}

$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];

$totalPages = ceil($totalRows/$perPage);
if($page > $totalPages){
    header('Location: product-list.php?page='. $totalPages);
    exit;
}


if(count($tags)==0) {
    $sql = sprintf("SELECT * FROM `products` %s %s LIMIT %s, %s",
        $where, $orderBySQL, ($page-1)*$perPage, $perPage);
} elseif(count($tags)==1) {

    $sql = sprintf("SELECT p.* FROM `products` p 
    JOIN
    (SELECT `product_sid` FROM `product_tags` WHERE tag_sid={$tags[0]}) t
    ON p.PrdID=t.product_sid
    %s %s LIMIT %s, %s",
        $where, $orderBySQL, ($page-1)*$perPage, $perPage);

    $urlParams['tags'] = $tags;
} else {
    $t_sql = "SELECT rs1.product_sid FROM (SELECT * FROM `product_tags` WHERE tag_sid={$tags[0]}) rs1 ";

    for($i=0; $i < count($tags)-1; $i++) {
        $tid = $tags[$i + 1];
        $rs = $i + 2;
        $t_sql .= " JOIN (SELECT * FROM `product_tags` WHERE tag_sid={$tid}) rs{$rs}
            ON rs1.product_sid=rs{$rs}.product_sid ";
    }

    $sql = sprintf("SELECT p.* FROM `products` p 
    JOIN
    (%s) t
    ON p.PrdID=t.product_sid
    %s %s LIMIT %s, %s",
        $t_sql, $where, $orderBySQL, ($page-1)*$perPage, $perPage);

    $urlParams['tags'] = $tags;
}

//$sql = sprintf("SELECT * FROM `products` %s %s LIMIT %s, %s",
//    $where, $orderBySQL, ($page-1)*$perPage, $perPage);

$rows = $pdo->query($sql)->fetchAll();


?>
<?php include __DIR__. '/__html_head.php'  ?>
<?php require __DIR__. '/__navbar.php'  ?>

<div class="container">

<div class="row">
    <div class="col">
        <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                <a class="page-link" href="?<?php
                $urlParams['page'] = $page-1;
                echo http_build_query($urlParams); ?>">Previous</a>
            </li>

            <?php for($i=$page-5; $i<=$page+5; $i++){ 
                if($i<1) continue;
                if($i>$totalPages) continue;
                $urlParams['page'] = $i;
                ?>
            <li class="page-item <?= $page==$i ? 'active' : '' ?>">
                <a class="page-link" href="?<?= http_build_query($urlParams) ?>"><?= $i ?></a>
            </li>
            <?php } ?>
            <li class="page-item <?= $page==$totalPages ? 'disabled' : '' ?>">
                <a class="page-link" href="?<?php
                $urlParams['page'] = $page+1;
                echo http_build_query($urlParams); ?>">Next</a>
            </li>
        </ul>
        </nav>
    </div>
    <div class="col">
        <form class="form-inline my-2 my-lg-0">
            <select class="form-control mr-sm-2" name="orderBy">
                <?php foreach($orderRules as $k=>$v): ?>
                <option value="<?= $k ?>" <?= $orderBy==$k ? 'selected' : '' ?>>
                    <?= $v['name'] ?></option>
                <?php endforeach; ?>
            </select>
            <select class="form-control mr-sm-2" name="type">
                <option value="PrdName" <?= $type=='PrdName' ? 'selected' : '' ?>>產品名搜尋</option>
                <option value="PrdNameInd" <?= $type=='PrdNameInd' ? 'selected' : '' ?>>Indonesian</option>
            </select>

            <input class="form-control mr-sm-2" type="search"
                   name="keyword" value="<?= htmlentities($keyword) ?>"
                   placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜尋</button>
            &nbsp;
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                進階搜尋
            </button>
        </form>

    </div>
</div>
<table class="table">
    <thead>
        <tr>
            <td><i class="fas fa-trash-alt"></i></td>
            <td>產品編號</td>
            <td>產品名</td>
            <td>長度</td>
            <td>寬度</td>
            <td>高度</td>
            <td>標準品</td>
            <td>產地</td>
            <td>價格</td>
            <td>圖</td>


            <td><i class="fas fa-edit"></i></td>
        </tr>
    </thead>
  <tbody>
  <!-- `PrdID`, `PrdName`, `PrdNameInd`, `Length`, `Width1`, `Width2`, `Width3`, `Width4`, `Height`, `thickness`, `LegW`, `SeatH`, `Weight`, `Cube`, `Standard`, `MadeIn`, `Cost`, `Price`, `Pricing`, `QtySafe` -->
      <?php foreach($rows as $r):
          if(!empty($r['images'])) {
              $images = json_decode($r['images'], true);
          } else {
              $images = [];
          }
          ?>
        <tr>
            <td><a href="javascript: del_it(<?= $r['PrdID'] ?>)"><i class="fas fa-trash-alt"></i></a></td>
            <td><?= $r['PrdID'] ?></td>
            <td><?= $r['PrdName'] ?></td>
            <td><?= $r['Length'] ?></td>
            <td><?= $r['Width1'] ?></td>
            <td><?= $r['Height'] ?></td>
            <td><?= $r['Standard'] ?></td>
            <td><?= $r['MadeIn'] ?></td>
            <td><?= $r['Price'] ?></td>
            <td>
                <?php if(!empty($images[0])): ?>
                    <img src="./upload-img/<?= $images[0] ?>" alt="" width="100px">
                <?php endif; ?>
            </td>
            <td>
                <a href="product-edit.php?PrdID=<?= $r['PrdID'] ?>">
                    <i class="fas fa-edit"></i>
                </a>
            </td>
        </tr>
        <?php endforeach ?>
  </tbody>
</table>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable ">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">進階搜尋</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="height: 1000px;">
<!--                    <iframe src="modal-search-tags.php" frameborder="0" width="100%" height="100%"></iframe>-->
                    <div class="form-group">
                        <form name="tag_form" action="product-list.php" target="_parent">
                            <select class="form-control mr-sm-2" name="orderBy">
                                <?php foreach($orderRules as $k=>$v): ?>
                                    <option value="<?= $k ?>" <?= $orderBy==$k ? 'selected' : '' ?>>
                                        <?= $v['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <select class="form-control mr-sm-2" name="type">
                                <option value="PrdName" <?= $type=='PrdName' ? 'selected' : '' ?>>產品名搜尋</option>
                                <option value="PrdNameInd" <?= $type=='PrdNameInd' ? 'selected' : '' ?>>Indonesian</option>
                            </select>

                            <input class="form-control mr-sm-2" type="search"
                                   name="keyword" value="<?= htmlentities($keyword) ?>"
                                   placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜尋</button>


                            <table class="table table-bordered table-striped">
                                <tbody>
                                <?php foreach ($tag_cates as $tg): ?>
                                    <tr>
                                        <td style="color:darkblue"><?= $tg['name'] ?></td>
                                        <td>
                                            <?php if (!empty($tg['children'])) foreach ($tg['children'] as $t): ?>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="tag<?= $t['sid'] ?>" name="tags[]"
                                                           value="<?= $t['sid'] ?>"
                                                        <?= in_array($t['sid'], $tags) ? 'checked' : '' ?>
                                                    >
                                                    <label class="form-check-label" for="tag<?= $t['sid'] ?>"><?= $t['name'] ?></label>
                                                </div>
                                            <?php endforeach; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__. '/__scripts.php'  ?>
<script>

    $('select[name=orderBy]').on('change', function(event){
        location.href = 'product-list.php?orderBy=' + $(this).val();
    });




    function del_it(id) {

        if(confirm('你要刪除編號為 ' + id + ' 的資料嗎？')){
            location.href = 'del-customer.php?id=' + id;
        }

    }

</script>
<?php include __DIR__. '/__html_foot.php'  ?>